package com.example.lukaszb.notessql;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,ListView.OnItemLongClickListener{
    private NotesSQLHelper hlp;
    private SQLiteDatabase db;
    private Button bDodaj;
    private ListView listView;
    private NotesAdapter adapter;
    private Cursor c;
    private int idToDelete=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        deleteDatabase("notes.db");
        hlp=new NotesSQLHelper(this);
        db=hlp.getWritableDatabase();

        bDodaj=findViewById(R.id.bDodaj);
        bDodaj.setOnClickListener(this);
        listView=findViewById(R.id.lvWpisy);
        c=hlp.getNotes(db);
        adapter=new NotesAdapter(this,c);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
    }

    public void saveNote(String note){
        ContentValues cv=new ContentValues();
        cv.put("entry",note);
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-mm-dd");
        String temp= spf.format(new Date());
        cv.put("data",temp );
        SQLiteDatabase db=hlp.getWritableDatabase();
        db.beginTransaction();
        try {
            db.insert(NotesSQLHelper.TABLE_NAME, null, cv);
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }

    @Override
    public void onClick(View view) {
        String text=((EditText)findViewById(R.id.editText)).getText().toString();

        if(text.length()>0){
            saveNote(text);
        }
        c=hlp.getNotes(db);
        adapter.changeCursor(c);
    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> adapterView, View view, int i, long l) {
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);
        idToDelete=i;
        alertDialogBuilder.setTitle("Potwierdź").setMessage("Czy chcesz usunąć bieżącą pozycję ?").
                setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hlp.deleteNote(db,((CursorAdapter)adapterView.getAdapter()).getItemId(idToDelete));
                        adapter.changeCursor(hlp.getNotes(db));
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).setIcon(android.R.drawable.alert_light_frame).show();

        return true;
    }


    private class NotesAdapter extends CursorAdapter{
        public NotesAdapter(Context context, Cursor c) {
            super(context, c, false);

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return LayoutInflater.from(context).inflate(R.layout.notes_item_layout,viewGroup,false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tvEntry=view.findViewById(R.id.tvEntry);
            TextView tvDate=view.findViewById(R.id.tvDate);
            tvEntry.setText(cursor.getString(cursor.getColumnIndex("entry")));
            tvDate.setText(cursor.getString(cursor.getColumnIndex("data")));
        }
    }
}
