package com.example.lukaszb.notessql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lukaszb on 05.12.2017.
 */

public class NotesSQLHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="notes.db";
    private static final int DB_VERSION=2;
    public static final String TABLE_NAME="notes";
    private static final String CREATE_STATEMENT="create table if not exists "+TABLE_NAME+" ("
            +"_id integer primary key,"
            +"entry text)";
    public NotesSQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       sqLiteDatabase.execSQL("drop table if exists "+TABLE_NAME);
       sqLiteDatabase.execSQL(CREATE_STATEMENT);
        ContentValues cv=new ContentValues();
        cv.put("entry","Test note");
        sqLiteDatabase.insert(TABLE_NAME,null,cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("alter table "+TABLE_NAME+" add column data text default '2017-12-20'");
    }

    public Cursor getNotes(SQLiteDatabase db){
        Cursor c=db.rawQuery("select * from notes",null);
        if(c!=null)
            c.moveToFirst();
        return c;
    }

    public void deleteNote(SQLiteDatabase db, long id){
        db.delete(TABLE_NAME,"_id=?",new String[]{Long.toString(id)});
    }
}